#ifndef LINEARALGEBRA_TEST_ARRAY_PARALLEL_UTIL_H
#define LINEARALGEBRA_TEST_ARRAY_PARALLEL_UTIL_H
#include <mpi.h>

namespace molpro {
namespace linalg {
namespace test {

extern MPI_Comm mpi_comm;

} // namespace test
} // namespace linalg
} // namespace molpro

#endif // LINEARALGEBRA_TEST_ARRAY_PARALLEL_UTIL_H
